const logger = require('./logger')
const mysql = require('mysql')
const {google} = require('googleapis')
const moment = require('moment')

module.exports = {
    
    addEvents:function(settings,auth) {
        const gmail = google.gmail({version: 'v1', auth})
        gmail.users.messages.list({
            'userId': 'me',
            'q': 'from:noreply@unive.it'
        }, (err, res) => {
            if (err) return logger.logError(settings.logPath,"Can't get email list because of " + err)
            else
            logger.logInfo(settings.logPath,"Emails retrieved")
            res.data.messages.forEach(message => gmail.users.messages.get({
                    'userId': 'me',
                    'id': message.id,
                    'format': 'full'
                }, (err, m) => {
                    if (err) return logger.logError(settings.logPath,"Can't get email because of " + err)
                    logger.logInfo(settings.logPath,"Email " + message.id + " retrieved")
                    
                    let subject = m.data.payload.headers.filter(header => header.name === 'Subject' 
                    && header.value.includes('Ricevuta prenotazione'))
    
                    if(subject.length != 0) {
                        let body                = m.data.snippet
                        let luogo               = subject[0].value.split('\"')[1]
                        let giorno              = body.split('Giorno: ')[1].split('Prenotazione: ')
                        let ora                 = giorno[1].split('Codice prenotazione: ')
                        let codice_prenotazione = ora[1].split('Codice di verifica: ')
                        let codice_verifica     = codice_prenotazione[1].split(' ')
                        let parsed_giorno = giorno[0].split(' ')[1].split('/')[2]+'-'+giorno[0].split(' ')[1].split('/')[1]+'-'+giorno[0].split(' ')[1].split('/')[0]
                        let parsed_data_inizio = ora[0].split('-')[0]
                        let parsed_data_fine = ora[0].split('-')[1]
                        let parsed_codice_prenotazione = ora[1].split('Codice di verifica: ')[0]
                        let parsed_codice_verifica     = codice_verifica[0]
                        let data_inizio = moment(new Date(parsed_giorno+' '+parsed_data_inizio),"DD MM YYYY HH:mm")
                        let data_fine = moment(new Date(parsed_giorno+' '+parsed_data_fine),"DD MM YYYY HH:mm")
                        let evento = {
                            'summary' : 'Studio in acquario',
                            'location': luogo,
                            'description': 'Prenotazione ' + luogo + '\nCodice prenotazione: ' + parsed_codice_prenotazione + '\nCodice verifica: ' + parsed_codice_verifica,
                            'start'   : {
                                'dateTime': data_inizio.toDate(),
                                'timeZone': 'Europe/Rome'
                            },
                            'end'     : {
                                'dateTime': data_fine.toDate(),
                                'timeZone': 'Europe/Rome'
                            },
                            'reminders':{
                                'useDefault': true
                            }
                        }
    
                        const calendar = google.calendar({'version': 'v3', auth})
                        calendar.calendarList.list((err, calendars) => {
                            if (err) return logger.logInfo(settings.logPath,"Can't access calendars because of " + err)
                            logger.logInfo(settings.logPath,"Calendars retrieved")
                            let id = calendars.data.items.filter(calendar => calendar.summary === 'Acquari')[0].id
                            calendar.events.list({
                                calendarId: id
                            }, (err,list) => {
                                if (err) return logger.logError(settings.logPath,"Can't access calendar "+id+" because of ", err)
                            logger.logInfo(settings.logPath,"Calendars Accessed")
                                let event_exists = false
                                list.data.items.forEach(date => {
                                    let start_date = moment(date.start.dateTime.toString(),"YYYY MM DD HH:mm").toDate()
                                    let end_date = moment(date.end.dateTime.toString(),"YYYY MM DD HH:mm").toDate()
    
                                    if (!event_exists)
                                        event_exists = start_date.toLocaleString() === evento.start.dateTime.toLocaleString() && 
                                                       end_date.toLocaleString() === evento.end.dateTime.toLocaleString()
                                })
                                
                                if (!event_exists) {
                                    calendar.events.insert({
                                        auth: auth,
                                        calendarId: id,
                                        resource: evento
                                    }, (err, event) => {
                                        if (err) return logger.logError(settings.logPath,"Can't add event " + event.data.id + "because of " + err)
                                        else {
                                            const connection = mysql.createConnection({
                                                host: settings.dbIpAddress,
                                                user: settings.dbLogName,
                                                password: settings.dbLogPasswd,
                                            })
                                            connection.connect((err) => {
                                                if (err) return logger.logError(settings.logPath, err)
                                                else{
                                                    logger.logInfo(settings.logPath,"Connection to database established")
                                                    let query = "INSERT IGNORE INTO calendar.events(id,subject,start_date,end_date,res_code,ver_code) VALUES ?"
                                                    let values = [
                                                        [event.data.id,event.data.summary,evento.start.dateTime,evento.end.dateTime,parsed_codice_prenotazione,parsed_codice_verifica]
                                                    ]
                                                    connection.query(query,[values], (err, result) => {
                                                        if (err) logger.logError(settings.logPath, err)
                                                        else {
                                                            logger.logInfo(settings.logPath,result)
                                                        }
                                                    })
                                                    connection.end()
                                                    return logger.logInfo(settings.logPath,"Event "+ event.data.id +"added to calendar")
                                                }
                                            })
                                        }
                                    })
                                    
                                }
                            })
                            
                        })
                    }
                }))
        })
    }
}