const fs       = require('fs')
const oAuth    = require('./modules/oAuth')
const logger   = require('./modules/logger')
const cron     = require('node-cron')


let settings = JSON.parse(fs.readFileSync("settings.json",(err,result) => {
    if (err) return console.error(err)
    else return result
}))

let addEvent = cron.schedule("*/5 * * * * *", () => {
    oAuth.activate(settings)
},{
    scheduled: true
})

let deleteLog = cron.schedule("5 * * * * *", () => {
    fs.unlink(settings.logPath, (err) => {
        if (err) logger.logError(settings.logPath,"Can't delete log file because of " + err)
    })
},{
    scheduled: true
})

addEvent.start()
deleteLog.start()
